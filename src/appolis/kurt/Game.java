package appolis.kurt;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

public class Game {
	public static void main(String[] args) {
		System.out.println("Welcome there you guys");
		String[] patterns = { "(a|b)", "(w)", "(\\w)",
				"(.*)", "(\\bcat\\b)","(\\d)", 
				"(\\d[0-9])", "(\\d{2,3})","(\\w)@(hotmail|gmail|rlabs).(com|org)" };
		String[] patterns_hints = { "Only 'a' or 'b' was allowed", "Only the letter 'w' was allowed", "Any character was allowed", "Any character including symbols is allowed", 
				"Specifically looking word 'cat'",
				"An integer was allowed", "An integer between 0 to 9", "Looking for an integer that is atleast two digits and no greater than three digits",
				"Any word without symbols that matches domains and extension, used for email address" };

		int score = 0;
		Scanner input_scanner = new Scanner(System.in);
		String input = "";
		Matcher m = null;
		Pattern r = null;
		for (int i = 8; i < patterns.length; i++) {
			System.out.println("Try and Match: " + patterns[i]);
			r = Pattern.compile(patterns[i]);

			input = input_scanner.next();
			m = r.matcher(input);
			if (m.find()){
				System.out.println("Right!");
				score++;
			}else{
				System.out.println("Wrong: "+patterns_hints[i]);
			}
		}
		System.out.println("Final Score : " + score+" out of "+patterns.length);

		input_scanner.close();
	}
}
